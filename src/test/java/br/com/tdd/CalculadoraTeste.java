package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumerosInteiros(){
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSomaDeDoisNumerosFracionados(){
        BigDecimal resultado = Calculadora.soma(2.2, 2.36);
        Assertions.assertEquals(BigDecimal.valueOf(4.56), resultado);
    }

    @Test
    public void testarSubtracaoDeDoisNumerosInteiros(){
        int resultado = Calculadora.subtrai(22, 3);
        Assertions.assertEquals(19, resultado);
//        AssertionError.class(Exception);
    }

    @Test
    public void testarSubtracaoDeDoisNumerosFracionados(){
        double resultado = Calculadora.subtrai(2, 0.5);
        Assertions.assertEquals(1.5, resultado);
//        AssertionError.class(Exception);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosInteiros(){
        int resultado = Calculadora.multiplica(7, 2);
        Assertions.assertEquals(14, resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosFracionados(){
        double resultado = Calculadora.multiplica(7.5, 2.5);
        Assertions.assertEquals(18.75, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosInteiros(){
        int resultado = Calculadora.divide(7, 2);
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosFracionados(){
        double resultado = Calculadora.divide(7.25, 0.4);
        Assertions.assertEquals(18.125, resultado);
    }

    @Test
    public void testarErroDivisaoPorZero(){
        Assertions.assertThrows(RuntimeException.class, () -> { Calculadora.divide(7.25, 0); });
    }
}
