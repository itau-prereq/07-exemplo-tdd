package br.com.tdd;

import java.math.BigDecimal;

public class Calculadora {

    public static int soma(int primeiroNumero, int segundoNumero){
        return primeiroNumero + segundoNumero;
    }

    public static BigDecimal soma(double primeiroNumero, double segundoNumero){
        return BigDecimal.valueOf(primeiroNumero).add(BigDecimal.valueOf(segundoNumero));
    }

    public static int subtrai(int primeiroNumero, int segundoNumero){
        return primeiroNumero - segundoNumero;
    }

    public static double subtrai(double primeiroNumero, double segundoNumero){
        return primeiroNumero - segundoNumero;
    }

    public static int multiplica(int primeiroNumero, int segundoNumero){
        return primeiroNumero * segundoNumero;
    }

    public static double multiplica(double primeiroNumero, double segundoNumero){
        return primeiroNumero * segundoNumero;
    }

    public static int divide(int primeiroNumero, int segundoNumero){
        if(segundoNumero == 0){
            throw new RuntimeException("Não é possivel dividir por 0");
        }
        return primeiroNumero / segundoNumero;
    }

    public static double divide(double primeiroNumero, double segundoNumero){
        if(segundoNumero == 0){
            throw new RuntimeException("Não é possivel dividir por 0");
        }
        return primeiroNumero / segundoNumero;
    }
}
